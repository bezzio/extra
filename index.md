# Next steps: analytics and CRM

## Summary

In the last quarter, two main issues have emerged:

- Demand for precise measurement of engagement metrics, usually surrounding the lead generation process
- To track the flow of a lead between the first engagement on the Store, trial signup, purchases, and possibly offline touchpoints, for a clear attribution of the lead or new customer.

The next paragraph will discuss the limitations of current tools. In the next, will be presented the first alternative, HubSpot, followed by a more online-centered, full-stack approach. Closing, a brief discussion of main pros and cons.

1. [Problem statement](#Problem Statement) 
2. [Hubspot](#HubSpot) 
3. [Analytics stack](#Analytics stack) 
4. [Comparison](#Comparison) 
5. [Case HubSpot: next step](#Case HubSpot: next step)

## Problem statement

Currently, our main tool for investigating user engagement is **Google Analytics**. Analytics excels in measuring aggregate metrics based on the _hit_ building block, such as pageviews, sessions, transactions. Born in the 90s, Analytics predate web-apps and their technologies (among which React), resulting in **tracking issues and a lower data reliability**. Secondly, the **high sampling rate** makes the user segmentation highly imprecise. 

Eventually, Analytics is not design, nor best suited, for the analysis of engagement and people metrics, itself the building block of (1) CRO activities and (2) correct attribution of leads and purchases among campaigns and channels.

Analytics data are partially integrated by Hotjar. The tool provides useful data from polls, while heatmaps can uncover some insights for content and page layout. However, the absence of a query tool and the one-shot, on-demand heatmap creation, make the tool unable to answer to quantitative questions.

## HubSpot

**HubSpot** is one of the most widely used inbound marketing platform in the web space, aiming to be a jack of all trades. HubSpot itself can be considered as a fully integrated stack comprising many tools.

At its core, the platform is built on (1) the **contacts database**, which powers the CRM, forms, email marketing, "lists" or customer segmentation, etc. and (2) the **CMS**, at the heart of the SEO tools, among which blog and landing pages, social media management and marketing, etc. HubSpot also includes a number of other tools, such as conversational bots, live chat, ad management, marketing automation, attribution reporting, a/b testing, custom event reporting.

As for the Store, the implementation may follow a similar plan:

- *Integrate forms and CTAs* directly in React (Tag managers won't help much), to collect users and leads data (including privacy, marketing and analytics flags)
- Upstream integration: *link ads platforms* to HubSpot, for (1) lead attribution and (2) send back custom audiences
- *Custom event tracking*: define which actions should be tracked, and implement event tags via GTM
- Downstream integration: for _purchases_, from BBS / Zuora / Reviso, to optimize the platforms' machine learning.
- Setup of single tools, as landing page, email marketing tool, chatbot and live chat.

**PROs**: first and foremost, HubSpot is a fully fledged **inbound marketing platform**, mainly thanks to the CRM, the CMS and the marketing automation. It is also perfectly fitted for **social media management**. Secondly, the **segmentation and attribution** tools will impact the creation, performance and analysis of campaigns both inside and across different channels (even if the attribution tool seems not a top priority in the platform). Thirdly, HubSpot contains a plethora of tools and integrate with most of the known tools, making it both a tool for **integrating and centralizing** most data, and a track / force toward integration, spanning across teams. Bonus: while it may not be intended, HubSpot can also perform (basic) engagement and people **analytics**, being based on users and thanks to the integration with ads, purchases and the additional event tracking.

**CONs**: the main issue we may encounter with HubSpot is its very centralization attitude. It performs the best when any and **all interactions** go through and **are tracked** by the platform. **Their CMS** is strongly **recommended**, and the integration with the current Store and Blog technology stacks may represent an implementation challenge. The centralization can also represent an issue if we intend to intentionally keep some data, if default integrations don't work or are not available, and if we want to **export data** and use / analyze it elsewhere. All in all, It also may takes a serious effort and a considerable time to be completely ready.

On the other hand, while being a jack of all trades, HubSpot may possibly be a master of none, at least for anything outside its main focus of inbound marketing. Specifically, the attribution report documentation lack examples of  conversion flows and other in-depth analyses such as multi-goal reports. The same may hold for the ads management tool, which does not include all the functionality of, thus does not substitute, the native Facebook / AdWords platforms. Also analyses of custom events may need a full extraction and a second tool/platform, pushing the need for a DB and some custom extraction / query even for straightforward reports.

## Analytics stack

In contrast to the SaaS solution of HubSpot, the main alternative considered here is a full-stack approach. The rationale is to look for the best tools in the market, tools that are often specialized in a single task along the ecommerce funnel.

### Tag management

Most of the tools that follow work on tags, and the data sent from them. The current tool is **Google Tag Manager**: a solution released by Google in 2012, part of their ads and analytics stack, free of charge but somehow limited in functionality. [Here](https://www.youtube.com/watch?v=9A-i7EWXzjs&list=PLI5YfMzCfRtYLtw_djEwG0nR-F9r6B5JT) some info on the product.

Other solutions may include Commanders Act's [TagCommander](https://www.commandersact.com/en/products/tagcommander/) and Tealium's [IQ Tag Management](https://tealium.com/products/tealium-iq-tag-management-system/). These alternatives distinguish themselves for the ease of use, the availability of embedded analytics on the tag performance (i.e. load and firing timings), and a higher control on the data the tag will send (the data layer). [Here](http://videos.tealium.com/watch/83FNtZpLN871eA6hifoXG8) an example of the Tealium solution.

### DMPs and CDPs

Built directly on top of tags, data management platforms or DMPs are tools designed for ingesting and integrate ads and tags data into a single, structured data warehouse. The ultimate goals are to (1) attribute conversions to the right channel and (2) to segment users and build audiences to be used inside ads platforms. They work on aggregate data, and are only useful inside ads platforms, being based on ad-tech stacks rather than a full web stack.

Customer Data Platforms, or CDPs, extend the DMP idea to offline touchpoints and, in general, a broader set of data sources, including personally identifiable information. In fact, while DMPs works on anonymous (groups of) users, due to compliancy with the different integrated platforms, CDPs works on the single, identified user, and single interactions, making for a high granularity optimal for attribution, conversion flows and custom analyses. 

The line between these two kinds of tools is a blurred one. [Here](https://www.treasuredata.com/learn/cdp-vs-dmp/) a blog post that discuss typical differences. Nowadays, true DMPs are usually offered as a compliment of Demand Side Platforms, while most historical suppliers includes more powerful tools like Commanders Act's [DataCommander](https://www.commandersact.com/en/products/datacommander/) and [Fusecommander](https://www.commandersact.com/en/products/fusecommander/) and Tealium's [DataAccess](https://tealium.com/products/tealium-dataaccess/) and [AudienceStream](https://tealium.com/products/audiencestream/) built on top of their tag management offerings.

More recent, well known players are [Segment](https://www.lytics.com/), [Arm Treasure Data](https://www.treasuredata.com/), [mParticle](https://www.mparticle.com/) and [Lytics](https://www.lytics.com/). These tools can be considered as CRMs entirely based on online and digital activities, with some retro-fitting. All performs similar jobs:

- they **connect to a large number of providers** (i.e. Segment connects to more than 200 services) like ads platform, email marketing platforms, subscription and payment services, but also event and engagement analytics platforms and CRMs. 
- Their intent is to strongly perform in **identity reconciliation**, as in track every single user and each of their interaction, primarily with a focus on online interactions but leaving more or less room for manual additions from the sales force and agents. 
- Eventually, these info are used in **advanced segments and building audiences**, in which these tools truly outstand competition. 
- As for analytics capabilities, each product has different approaches, but they tend to dump everything in a data lake owned by the customers, and let them use their tool of choice.

Among these new platforms, attribution, one of the basic tasks of traditional DMPs, is performed at the most granular level - the user -  instead of a campaign or channel base. This uncover the need for a different level of analysis of campaigns and channel performance: it can be both undertaken in an offline fashion, with custom queries and excel files, or with dedicated tools, such as [AttributionApp](https://www.attributionapp.com/).

### Engagement and people analytics

As for DMPs and CDPs, web analytics tools evolved over time. In the 90s, Google and Adobe Analytics has arisen to industry standards, tracking basic HTML pages and sites for pageviews, users and events. In the last years, the diffusion of different technologies oriented to web apps, such as Angular, React and Vue highlighted the need for tools focused on users and their interactions, with little to no focus on single pageviews - being it almost disappeared.

**Mixpanel** is the oldest of these new kind of instruments. Its goal is to **identify and track users across their journey,** helping in the **analysis of customer engagement, retention, flows and funnels**. It supports natively most of web apps, also providing specific tools as a/b testing, messages and push notification. Its implementation follows a path in the likes of:

- Identify important events and interactions in which the user identify himself
- Implement the needed tracking tags, separately tracking engagements and user identification
- Analyze the produced data, learn and reiterate if necessary
- Analyze user behavior and build reports and dashboards

Mixpanel naturally complement Google Analytics data, with the former focused on qualitative data and specific, engagement-related metrics, while the latter focus on aggregate and overall quantitative data. 

**Heap Analytics** is a more recent pivot of the Mixpanel approach. As for the interface, Heap is somehow less user-friendly, presenting various sections each one specialized in a different usage and query; **being more complex, the interface is also more powerful**, letting the user respond to questions that former tool won't answer without advanced techniques. On the technical side, the main advantage of Heap is its auto-tracking ability: once the main snippet is installed, all data will be automatically recorded; the user will select the events he's interested in later, directly inside the analytics platform. Heap analytics is possibly the only tool here presented with this capability, while Mixpanel suppressed this feature earlier in 2018.

**Amplitude** instead represent a **compromise** between these two tools. It has not the auto-tracking feature but still it seems more powerful, as for UI queries, than Mixpanel. However, it is the most complex to integrate and deploy.

As for prices, Heap is by far the most expensive tool, leveraging its features, starting at 300$ per 25k sessions (going for the k€ range per month). Mixpanel may be the cheapest one, with an estimate annual cost under the 5k€. Amplitude has a large free tier level (10M data points per month) possibly making for a free tool in this cases.

### Building the stack

The final result should comprise a tag management solution, a CDP and two analytics tools, for quantitative and qualitative data. A first example may be a stack containing Google Tag Manager, Mixpanel, Google Analytics and Segment; or alternatively, by Tealium (IQ Tag Management), Mixpanel, Google Analytics, Tealium DataAccess and AudienceStream. 

As for the choice of the various tools, two elements are fundamental: they should individually meet the business requirements, but it is also important to take into account how easily they can be connected and integrated. Even more, they should integrate with the current technologies, i.e. SendGrid, Intercom, Calendly, Mailup and MailChimp etc.

## Comparison

Both alternatives present some implementation challenges, and they both may require 1/2 months to fully deploy. 

HubSpot has the great advantage of being (1) a single ecosystem for most of our needs, it is specifically design to (2) work cross-teams, and come with the (3) "additional" inbound marketing tooling. However, we'll eventually outgrown the single platform, and will emerge the need for additional capabilities, possibly in the attribution and the CRO area. We'll also be overlooking some of their most famous and valued tools, i.e. the CMS, the blogging platform and other SEO tools, while the extraction of data - to be analyzed and used elsewhere - will represent some issue and costs.

The full-stack approach here presented is the minimum ensemble required by working with a "separate" digital sales team, for solving the engagement analytics issue and to correctly attribute individual conversions. It is clearly more fragmented, possibly more complex to use/implement, and probably more costly - additionally requiring a different email marketing and inbound marketing tool. 

It also has two fundamental advantages: it truly ensures the sourcing of (1) the best tool for the job, and it is completely designed, down to the details of the individual tool, to work as an orchestrated stack - meaning that (2) the data ownership and control will be ours, ensuring the ability to analyze and use it everywhere. It can also be considered future-proof, by avoiding any vendor look-in, and providing a backbone for any additional tool we may need in the future.

## Case HubSpot: next step

1. Acquire access to the platform
2. Setup (1) forms and (2) CTAs, implement them into the React codebase
3. Define events to be tracked; implement their tags via GTM
4. Import UTM tags currently used
5. Create campaigns object to be linked with the current activities
6. Collect and import known contacts
7. Connect the various ad platforms
8. Build the landing pages
9. Connect social pages
10. Define and insert marketing automation rules
11. Define custom audiences and deploy them to the ads platforms
12. Define, implement and automate reporting